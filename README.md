# eslint-config-bic
Правила eslint для js и ts файлов
Включает в себя следующие правила:
- [eslint:recommended](https://eslint.org/docs/rules/)
- [eslint-config-aribnb-base](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base)
- [jquery/deprecated](https://github.com/dgraham/eslint-plugin-jquery)
- [you-dont-need-lodash-underscore/compatible](https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore)

## Usage
1. Install 
````sh
yarn add eslint-config-bic -D -E
````

2. Add `"extends": "bic"` to your .eslintrc.

