module.exports = {
	extends: [
		'eslint:recommended',
		'plugin:import/errors',
		'plugin:import/warnings',
		'plugin:import/typescript',
		'plugin:jquery/deprecated',
		'plugin:you-dont-need-lodash-underscore/compatible',
		'plugin:sonarjs/recommended',
		'airbnb-base',
		'.eslintrc.yaml'
	],
};
